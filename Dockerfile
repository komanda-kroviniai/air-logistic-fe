FROM mantasgg/node-chrome:v1
  WORKDIR /usr/src/app
  COPY package.json package-lock.json ./
  RUN npm ci
  COPY . .
  RUN apk add chromium
  RUN apk add chromium-chromedriver
  ENV CHROME_BIN=/usr/bin/chromium-browser
  ENTRYPOINT npm run test
