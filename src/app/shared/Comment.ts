export interface Comment{
  id: number;
  forumId: number;
  comment: string;
  createdBy: string;

}
