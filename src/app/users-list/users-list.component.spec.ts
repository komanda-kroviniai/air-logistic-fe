import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HttpClientModule } from '@angular/common/http';
import {UsersListComponent} from "./users-list.component";
import {ReactiveFormsModule} from "@angular/forms";
import {RoutesComponent} from "../routes/routes.component";
import {RouterModule} from "@angular/router";

describe('UsersListComponent', () => {
  let component: UsersListComponent;
  let fixture: ComponentFixture<UsersListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UsersListComponent, RoutesComponent ],
      imports: [ HttpClientModule, ReactiveFormsModule, RouterModule.forRoot([]) ] // Add HttpClientModule here
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
